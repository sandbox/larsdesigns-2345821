Report displaying Git information as a Drupal report. Report includes git
status, git show and git log entries. The report refreshes each time the URL is
requested.

INSTALLATION
Use standard Drupal module installation instruction and best practices. Enable
this module and view the report.

Visit the URL: admin/reports/git-info to view the report or browser the
admin menu -> reports -> git-info.
